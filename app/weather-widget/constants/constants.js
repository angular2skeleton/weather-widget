"use strict";
exports.FORECAST_KEY = "e7bbe1bb376120535a11551697518109";
exports.FORECAST_ROOT = "https://api.darksky.net/forecast/";
exports.GOOGLE_KEY = "AIzaSyAhflO4mQTKoa7KRpS9hS97DM46Cqdei2I";
exports.GOOGLE_ROOT = "https://maps.googleapis.com/maps/api/geocode/json";
exports.WEATHER_COLORS = {
    'default': {
        'background-color': '#fff',
        'color': '#000'
    },
    'clear-day': {
        'background': 'URL(app/weather-widget/images/clear-day.jpg) no-repeat center',
        'background-size': 'cover',
        'color': '#000'
    },
    'clear-night': {
        'background': 'URL(app/weather-widget/images/clear-night.jpg) no-repeat center',
        'background-size': 'cover',
        'color': '#000'
    },
    'rain': {
        'background': 'URL(app/weather-widget/images/rain.jpg) no-repeat center',
        'background-size': 'cover',
        'color': '#000'
    },
    'snow': {
        'background': 'URL(app/weather-widget/images/snow.jpg) no-repeat center',
        'background-size': 'cover',
        'color': '#000'
    },
    'sleet': {
        'background': 'URL(app/weather-widget/images/sleet.jpg) no-repeat center',
        'background-size': 'cover',
        'color': '#000'
    },
    'wind': {
        'background': 'URL(app/weather-widget/images/wind.jpg) no-repeat center',
        'background-size': 'cover',
        'color': '#000'
    },
    'fog': {
        'background': 'URL(app/weather-widget/images/fog.jpg) no-repeat center',
        'background-size': 'cover',
        'color': '#000'
    },
    'cloudy': {
        'background': 'URL(app/weather-widget/images/cloudy.jpg) no-repeat center',
        'background-size': 'cover',
        'color': '#000'
    },
    'partly-cloudy-day': {
        'background': 'URL(app/weather-widget/images/partly-cloudy-day.jpg) no-repeat center',
        'background-size': 'cover',
        'color': '#000'
    },
    'partly-cloudy-night': {
        'background': 'URL(app/weather-widget/images/partly-cloudy-night.jpg) no-repeat center',
        'background-size': 'cover',
        'color': '#000'
    }
};
//# sourceMappingURL=constants.js.map